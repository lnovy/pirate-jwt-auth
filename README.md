
# REST server for SPA apps authentication

Allow login for SPA using JWT.
Uses DB with [phpbb schema](https://wiki.phpbb.com/Table.phpbb_users).

## Configuration

Config is performed through few environment variables:

- TOKEN_VALIDITY_IN_MINS: duration of JWT token in minutes (default 24h)
- SERVER_SECRET: random string the most secret string of da world
- DATABASE_URL: connection string to DB
- ALLOWED_ORIGIN: (optional, default \*.pirati.cz) allowed CORS origins
- PORT: desired port for listening
- NODE_ENV: should be 'production' for production environment

## Routes provided

- /login : POST (username, password), performs local users login
- /user/:uid : GET, returns info about user with given uid

## possible chages

Usage of different [signing algorithm](https://github.com/auth0/node-jsonwebtoken#algorithms-supported).
