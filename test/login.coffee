
chai = require('chai')
should = chai.should()
jwt = require('jsonwebtoken')
db = require('../lib/db')

module.exports = (g)->

  addr = g.baseurl

  describe 'login', ->

    beforeEach (done)->
      db.migrate.rollback().then ()->
        return db.migrate.latest()
      .then ()->
        return db.seed.run()
      .then ()->
        done()
      .catch (err)->
        done(err)

    afterEach (done)->
      db.migrate.rollback().then ()->
        done()
      .catch (err)->
        done(err)

    it 'must NOT return user and token with INvalid credentials', (done) ->
      chai.request(g.baseurl)
      .post('/login')
      .send({ username: 'gandalf', password: 'invalid' })
      .end (err, res) ->
        res.should.have.status(400)
        res.should.be.json
        should.not.exist(res.body.token)
        should.not.exist(res.body.user)
        done()

    it 'must return user and token with valid credentials', (done) ->
      chai.request(g.baseurl)
      .post('/login')
      .send({ username: 'gandalf', password: 'verysecretwhisper' })
      .end (err, res) ->
        return done(err) if err
        res.should.have.status(200)
        res.should.be.json
        res.body.token.should.be.ok
        jwt.verify res.body.token, process.env.SERVER_SECRET, (err, decoded) ->
          done(err) if err
          decoded.username.should.eql 'gandalf'
          decoded.email.should.eql 'gandalf@shire.me'
          should.not.exist(decoded.password)
          done()

    it 'must return user\'s profile', (done) ->
      chai.request(g.baseurl)
      .get('/profile/' + 1)
      .end (err, res) ->
        return done(err) if err
        res.should.have.status(200)
        res.should.be.json
        res.body.username.should.eql 'saruman'
        should.not.exist(res.body.password)
        done()
