fs = require('fs')
bodyParser = require('body-parser')
express = require('express')
chai = require('chai')
chaiHttp = require('chai-http')
chai.use(chaiHttp)
should = chai.should()

process.env.SERVER_SECRET = 'fhdsakjhfkjal'
# process.env.DATABASE_URL = 'sqlite://db.sqlite'
port = process.env.PORT || 3333
g = {}

# entry ...
describe 'app', ->

  g.app = app = require('../lib')

  before (done) ->
    this.timeout(5000)
    # init server
    g.server = app.listen port, (err) ->
      return done(err) if err
      done()

  after (done) ->
    g.server.close()
    done()

  it 'should exist', (done) ->
    should.exist g.app
    done()

  # run the rest of tests
  g.baseurl = "http://localhost:#{port}"

  submodules = [
    './login'
  ]
  for i in submodules
    SubMod = require(i)
    SubMod(g)
