exports.up = (knex, Promise) ->
  return knex.schema.createTable 'phpbb_users', (table) ->
    table.increments('user_id')
    table.string('username').notNullable().unique()
    table.integer('group_id').notNullable()
    table.integer('user_email').notNullable()
    table.string('user_avatar')
    table.string('user_password').notNullable()

exports.down = (knex, Promise) ->
  return knex.schema.dropTable('phpbb_users')
