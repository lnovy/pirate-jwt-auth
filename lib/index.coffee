express = require('express')
bodyParser = require('body-parser')
cors = require('cors')
db = require('./db')
loginCtrl = require('./login')
profileCtr = require('./profile')

# create app
module.exports = app = express()

# init CORS
opts =
  maxAge: 86400
  origin: process.env.ALLOWED_ORIGIN || /pirati\.cz$/
app.use(cors(opts))

# use JSON body parser for parsing credentials upon POST login
app.use bodyParser.json()

# setup login route
app.post '/login', loginCtrl(db.models.User)
# profile route
app.get '/profile/:uid', profileCtr(db.models.User)
