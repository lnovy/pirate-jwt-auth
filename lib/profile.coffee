Utils = require('./utils')

_fetchedCols = [
  'username', 'user_email', 'user_avatar'
]

module.exports = (User) ->

  return (req, res) ->
    User.where('user_id', req.params.uid).fetch(columns: _fetchedCols)
    .then (user)->
      if not user
        return res.status(404)
      res.json user.toJSON()
    .catch (err)->
      res.status(400).json(message: err)
