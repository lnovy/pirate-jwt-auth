jwt = require 'jsonwebtoken'
Utils = require('./utils')

tokenExpiresIn = parseInt(process.env.TOKEN_VALIDITY_IN_MINS) * 60 || 24* 60 * 60
console.log "token validity interval: #{tokenExpiresIn} secs"

_getToken = (user) ->
  tokencontent = JSON.parse(JSON.stringify(user))
  return jwt.sign(tokencontent,
    process.env.SERVER_SECRET,
    expiresIn: tokenExpiresIn
  )

_fetchedCols = [
  'user_id', 'username', 'group_id', 'user_email', 'user_avatar', 'user_password'
]

module.exports = (User) ->

  return (req, res) ->
    User.where('username', req.body.username).fetch(columns: _fetchedCols)
    .then (user)->
      if not user
        return res.status(400).json(message: 'BAD_CREDENTIALS')
      user = user.toJSON()
      if(! Utils.verifyPasswd(user.user_password, req.body.password))
        return res.status(400).json(message: 'BAD_CREDENTIALS')

      myuser =
        id: user.user_id
        username: user.username
        email: user.user_email
        avatar: user.user_avatar
        gid: user.group_id

      res.json
        user: myuser
        token: _getToken(myuser)
    .catch (err)->
      res.status(400).json(message: err)
