Utils = require('../utils')

exports.seed = (knex, Promise) ->
  return knex('phpbb_users').del() # del existing
  .then ()->
    return knex('phpbb_users').insert
      user_id: 0
      username: 'gandalf'
      group_id: 0
      user_email: 'gandalf@shire.me'
      user_avatar: 'http://gandalg.com/avatar.jpg'
      user_password: Utils.createPasswdHash('verysecretwhisper')
  .then ()->
    return knex('phpbb_users').insert
      user_id: 1
      username: 'saruman'
      group_id: 1
      user_email: 'saruman@mordor.me'
      user_avatar: 'http://saruman.com/avatar.jpg'
      user_password: Utils.createPasswdHash('verysecretbadword')
