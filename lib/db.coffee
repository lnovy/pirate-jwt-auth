Knex = require('knex')

debugopts =
  client: 'sqlite3'
  connection:
    filename: ':memory:'
  # debug: true
  migrations:
    directory: __dirname + '/migrations'
  seeds:
    directory: __dirname + '/seeds'

opts =
  client: 'mysql'
  connection: process.env.DATABASE_URL

knex = if process.env.NODE_ENV == 'production' then Knex(opts) else Knex(debugopts)
bookshelf = require('bookshelf')(knex)

User = bookshelf.Model.extend
  tableName: 'phpbb_users'

knex.models =
  User: User

module.exports = knex
