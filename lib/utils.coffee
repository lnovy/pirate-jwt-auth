crypto = require('crypto')

exports.createPasswdHash = (passwd)->
  md5sum = crypto.createHash('md5')
  md5sum.update(passwd)
  return md5sum.digest('hex')

exports.verifyPasswd = (hash, rawpasswd)->
  md5sum = crypto.createHash('md5')
  md5sum.update(rawpasswd)
  return md5sum.digest('hex') == hash
